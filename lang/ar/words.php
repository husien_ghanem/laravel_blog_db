<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'dashboard' => 'لوحة التحكم',
    'settings' => 'الاعدادات',
    'translations'=>'الترجمات',
    'users'=>'المستخدمين',
    'not activated' => 'غير مفعل',
    'admin'=>'مدير الموقع',
    'writer'=>'كاتب',
    'add user'=>'اضافة مستخدم',
    'main category'=>'قسم رئيسي',
    'posts'=>'المقالات',
    'add post'=>'اضافة مقالة',
    'categories'=>'الأقسام',
    'add category'=>'إضافة قسم',
    'image'=>'الصورة',
    'status'=>'الحالة',
    'title'=>'العنوان',
    'content'=>'الوصف',
    'name'=>'الإسم',
    'email'=>'البريد الإلكتروني',
    'password'=>'رمز المرور',
    'facebook'=>'فيسبوك',
    'instagram'=>'انستاغرام',
    'phone'=>'الهاتف',
    'email'=>'الايميل',
    'Logout'=>'خروج',
    'delete'=>'حذف',
    'close'=>'اغلاق',
    'sure'=>'متأكد',
    'add post'=>'إضافة مقالة',
    'edit post'=> 'تعديل المقالة',
    'smallDesc'=>'وصف قصير',
    ''=>'',
    'words.tags'=>'العلامات',
    'user settings'=>'إعدادات المستخدم',
    'logout'=>'تسجيل خروج',
    'home'=>'الرئيسية',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',
    ''=>'',

];

<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class settingResource extends JsonResource
{
    
    public function toArray(Request $request)
    {
        // return parent::toArray($request);
        $data = 
        [ 
            'logo' => $this->logo ,
            'faveicon'=>asset($this->favicon),
            'created'=>$this->created_at->format('Y-M-D'),
            "instagram"=>$this->instagrm,
            "phone"=>$this->phone,
            "email"=>$this->email,
            "title"=>$this->title,
            "content"=>$this->content,
            "address"=>$this->address,


        ];

        return $data ;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{

    public function toArray(Request $request)
    {
        // return parent::toArray($request) ;
      return
        [ 
            "id" => $this->id ,
            'image' => asset($this->image)  ,
            'parent' => $this->parent ,
            'created_at' => $this->created_at->format("Y-M-D") ,
            'title' => $this->title ,
            'content' => $this->content ,
            // 'children' =>$this->children
            'children' => CategoryCollection::make($this->whenLoaded('children')), 
            'posts' => PostResource::collection($this->whenLoaded('posts')), 
            //   PostResourcهنا 
        ];
     
    }
}
     // 'updated_at' => $this->updated_at->format("Y-M-D") ,
            // 'deleted_at' => $this->deleted_at->format("Y-M-D"),null
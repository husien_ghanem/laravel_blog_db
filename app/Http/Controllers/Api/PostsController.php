<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Policies\PostPolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\Return_;

class PostsController extends Controller
{

    public function index(Request $request)
    {
        $validator = validator::make($request->all(), [
            'category_id' => 'required|integer|exists:categories,id',
        ]
        );

        if ($validator->fails()) {
            return response()->json(['data' => $validator->getMessageBag()]);
        }

        $posts = Post::with('category')->where('category_id', $request->category_id)->paginate(4);
        return PostResource::collection(($posts));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {

        $post = Post::where('id',$id)->with('category')->firstOrFail();
        return PostResource::make($post) ;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

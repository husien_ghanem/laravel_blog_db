<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CategoryCollection;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategroriesController extends Controller
{

    public function index()
    {
        $categories = Category::paginate(2);
        return CategoryResource::collection($categories);
    }



    public function show($id)
    {
        // $category = Category::where('id', $id)->firstOrFail();
        // return CategoryResource::make($category);

        // $category = Category::findOrFail($id);
        //  return CategoryResource::make($category);

         try {
            $category = Category::findOrFail($id);
            return CategoryResource::make($category);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Category not found'], Response::HTTP_NOT_FOUND);
        }
    }






    public function navbarCategories()
    {
         $categories = Category::with('children')->where('parent' , 0)->orWhere('parent' , null)->get();
        // return  CategoryResource::collection($categories) ;
             return new CategoryCollection($categories) ;
    }



    public function indexPageCategoriesWithPost()
    {
        $categories_with_posts = Category::with(['posts'=>function ($q){
            $q->with('user')->limit(2);
        }])->get();

        return new CategoryCollection($categories_with_posts) ;
    }
}

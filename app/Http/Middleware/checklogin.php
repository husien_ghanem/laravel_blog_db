<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class checklogin
{
   
    public function handle(Request $request, Closure $next): Response
    {    $user_status=['admin','writer'];
    //    dd(auth()->user()->status);
        if(!in_array(auth()->user()->status,$user_status)){
            Auth::logout();
            return  redirect()->route('login');
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckLanguageApi
{
 
    public function handle(Request $request, Closure $next): Response
    {
           $language = array_keys(config('app.languages'));
        if(request()->hasHeader('lang') && in_array($request->header('lang'),$language)){
            // dd($request->header('lang'));
            app()->setlocale($request->header('lang')) ;
        }
        return $next($request);
    }
}

<?php

use App\Http\Controllers\Api\CategroriesController;
use App\Http\Controllers\Api\PostsController;
use App\Http\Controllers\Api\SettingController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return 2;
});

Route::get('/settings', [SettingController::class, 'index']);
Route::get('/nav-categories', [CategroriesController::class, 'navbarCategories']);
Route::get('indexPageCategoriesWithPost', [CategroriesController::class, 'indexPageCategoriesWithPost']);


Route::get('/categories', [CategroriesController::class, 'index']);
Route::get('/categories/{id}', [CategroriesController::class, 'show']);



Route::apiResource('posts',PostsController::class)->except(['update','store','destroy']);